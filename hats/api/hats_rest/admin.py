from django.contrib import admin
from .models import Hat, LocationVo

# @admin.register(Hat)
# class HatAdmin(admin.ModelAdmin):
#     list_display = (
#         "fabric",
#         "style_name",
#         "color",
#         "picture",
#     )

admin.site.register(Hat)
admin.site.register(LocationVo)