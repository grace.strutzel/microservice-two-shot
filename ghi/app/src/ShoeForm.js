import React from 'react';

class ShoeForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            manufacturer: '',
            model_name: '',
            color: '',
            picture_url: '',
            bin: '',
            bins: []
        };
        this.handleChange = this.handleChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state };
        delete data.bins;
        console.log(data)

        const shoeUrl = 'http://localhost:8080/api/shoes/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        }
        const response = await fetch(shoeUrl, fetchConfig);
        if (response.ok) {
            const newShoe = await response.json();
            console.log(newShoe)

            const cleared = {
                manufacturer: '',
                model_name: '',
                color: '',
                picture_url: '',
                bin: '',
            }
            this.setState(cleared)
        }
    }


    async componentDidMount() {
        const url = "http://localhost:8100/api/bins/"
        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json();
            this.setState({ bins: data.bins })
        }
    }

    handleChange(event) {
        const object = {}
        object[event.target.name] = event.target.value
        this.setState(object)
    }



    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="offset-3 col-6">
                        <div className="shadow p-4 mt-4">
                            <h1>Create a new presentation</h1>
                            <form onSubmit={this.handleSubmit} id="create-shoe-form">
                                <div className="form-floating mb-3">
                                    <input value={this.state.manufacturer} onChange={this.handleChange} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control" />
                                    <label htmlFor="manufacturer">Manufacturer</label>
                                </div>
                                <div className="form-floating mb-3">
                                    <input value={this.state.model_name} onChange={this.handleChange} placeholder="Model Name" required type="text" name="model_name" id="model_name" className="form-control" />
                                    <label htmlFor="model_name">Model Name</label>
                                </div>
                                <div className="form-floating mb-3">
                                    <input value={this.state.color} onChange={this.handleChange} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                                    <label htmlFor="color">Color</label>
                                </div>
                                <div className="form-floating mb-3">
                                    <input value={this.state.picture_url} onChange={this.handleChange} placeholder="Picture Url" required type="url" name="picture_url" id="picture_url" className="form-control" />
                                    <label htmlFor="picture_url">Picture Url</label>
                                </div>
                                <div className="form-floating mb-3">
                                    <select value ={this.state.bin} onChange={this.handleChange} required name="bin" id="bin" className="form-select">
                                        <option value=''>Choose a bin</option>
                                        {this.state.bins.map(bin => {
                                            return (
                                                <option key={bin.id} value={bin.href}>
                                                    {bin.bin_number}
                                                </option>
                                            )
                                        })}
                                    </select>
                                </div>
                                <button className="btn btn-primary">Create</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default ShoeForm
