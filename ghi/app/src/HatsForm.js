import React from 'react';

class HatsForm extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      style_name: '',
      fabric: '',
      color: '',
      picture: '',
      location: '',
      locations: []
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  async componentDidMount() {
    const url = 'http://localhost:8100/api/locations/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      this.setState({ locations: data.locations });
    }
  }

  async handleSubmit(event) {
    event.preventDefault();
    const data = {...this.state};
    delete data.location;

    const hatUrl = 'http://localhost:8090/api/hats/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(hatUrl, fetchConfig);
    if (response.ok) {
      const newHat = await response.json();
      console.log(newHat);

      const cleared = {
        style_name: '',
        fabric: '',
        color: '',
        picture: '',
        location: '',
      };
      this.setState(cleared);
    }
  }

  handleChange(event) {
    const object = {}
    object[event.target.name] = event.target.value
    this.setState(object)
  }

  render() {
    return (  
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new Hat</h1>
            <form onSubmit={this.handleSubmit} id="create-hat-form">
              <div className="form-floating mb-3">
                <input value={this.state.style_name} onChange={this.handleChange} placeholder="Style Name" required type="text" name="style_name" id="style_name" className="form-control" />
                <label htmlFor="style_name">Style Name</label>
              </div>
              <div className="form-floating mb-3">
                <input value={this.state.fabric} onChange={this.handleChange} placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control" />
                <label htmlFor="fabric">Fabric</label>
              </div>
              <div className="form-floating mb-3">
                <input value={this.state.color} onChange={this.handleChange} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                <label htmlFor="color">Color</label>
              </div>
              <div className="mb-3">
                <select value={this.state.location} onChange={this.handleChange} required name="location" id="location" className="form-select">
                  <option value="">Choose a Location</option>
                  {this.state.location.map(location => {
                    return (
                      <option key={location.id} value={location.id}>
                        {location}
                      </option>
                    );
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default HatsForm;