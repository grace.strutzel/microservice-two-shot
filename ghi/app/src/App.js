import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import HatsList from './HatsList';
import HatsForm from './HatsForm';
import ShoeList from './ShoeList';
import ShoeForm from './ShoeForm';


function App(props) {
  if (props.hats === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="hats" element={<HatsList hats={props.hats}/>} />
          <Route path="hats">
            <Route path="new" element={<HatsForm />} />
          </Route>
          <Route path="shoes">
            <Route path="" element={<ShoeList/>}/>
          </Route>
          <Route path="shoes">
            <Route path="new" element={<ShoeForm/>}/>
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
