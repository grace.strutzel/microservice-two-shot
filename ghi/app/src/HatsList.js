function HatsList(props) {
    return (
        <div>
            <h1>Hats</h1>
            <table className="table table-striped">
                <thead>
                <tr>
                    <th>Style name</th>
                    <th>Location</th>
                </tr>
                </thead>
                <tbody>
                {props.hats.map(hat => {
                    return (
                    <tr key={ hat.href}>
                        <td>{ hat.style_name }</td>
                        <td>{ hat.location }</td>
                    </tr>
                    );
                })}
                </tbody>
            </table>
        </div>
    );
  }
  
  export default HatsList;