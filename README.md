# Wardrobify

Team:

* Sharda - Shoes
* Adrien - Shoes
* Grace - Hats

## Design

## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.

## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.
*Add app into the project settings
*Model - Hat class (fabric, style_name, color, picture, location)
       - LocationVo class (import_href, closet_name, section_number, shelf_number)
*Register in admin
*Add app url into project urls
*Create views - encoders (LocationVODetailEncoder, HatListEncoder, HatDetailEncoder[location encoders])
    - list and create hats
    - delete, update, detail hats
*Add view urls into app
*Create poller to get data from wardrobe

